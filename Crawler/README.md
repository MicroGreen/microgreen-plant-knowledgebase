# Crawler 

## Description
This webcrawler is responsible for acquiring data and store it into the couchbase. The data is crawled from the website ```https://garden.org/``` via JSOUP. It is accessible over a spark-java website.

## Configuration 
The database needs to be configured before starting the crawler.
```cd Crawler\src\main\resources```  ```config.properties```

The requirements are a couchbase database, an empty bucket and an user with reading and writing privileges.

## Test
```cd Crawler```

```mvn surefire:test```

### Test-Report
```cd Crawler```

```mvn surefire-report:report```

## Run Crawler

### Build Jar
```cd Crawler```

```mvn package```

### Execute Jar:
```cd Crawler/target```

```java -jar Diplomprojekt-Webcrawler-1.0-SNAPSHOT-pkg.jar```

### Execute with Maven
```cd Crawler```

```mvn exec:java```

### Start Crawler

Visit ```localhost:4567/startCrawler``` this starts the crawling-process. You can close the browser session - the crawler will continue crawling automatically. 
If you want to quickcheck if the services is running visit ```localhost:4567/hello``` - this should return "Hi Adrian".

## Deploy Crawler

### Deploy on Heroku 

After creating a new app in heroku named 'plant-crawler' the following steps have to be done:
```heroku login```

```heroku git:clone -a plant-crawler```

```git add .```

```git commit -am "your commit"```

```git subtree push --prefix Crawler heroku master```

If you have different heroku apps located on master, make sure that you change the remote before pushing.

```git remote set-url heroku https://git.heroku.com/plant-crawler.git ```

### Prepare Docker
Before building the [Dockerfile](https://gitlab.com/MicroGreen/microgreen-plant-knowledgebase/blob/master/Crawler/Dockerfile) it is necessary to build a new Jar File with ```mvn package```.

The docker image can be created running the following command:
```docker build --tag=plantcrawler .```

### Deploy on Linux-Server
After the docker image can be created it can be started on a linux server with docker.

```docker run -p 8082:4567 -d --name plantcrawler plantcrawler```

After that the Service can be visited on the sites: 
```http://microgreen.meschareth.net:8082/hello``` - returns "Hi Adrian."

```http://microgreen.meschareth.net:8082/startCrawler``` - @Policy: Don't start without Adrians Permission. Starts Crawler, the Session can be closed and the Crawler continues. 


