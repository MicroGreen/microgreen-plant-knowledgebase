import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.nodes.Document;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.io.File;
import java.io.RandomAccessFile;


public class Webcrawler {
    static String regex = "^https:\\/\\/garden.org\\/plants\\/group\\/\\w";
    static String regex2 = "^https:\\/\\/garden.org\\/plants\\/browse\\/plants\\/children\\/";
    static String regex3 = "^https:\\/\\/garden.org\\/plants\\/view\\/.*";
    public static List<String> linkList;
    public static List<String> linkList2;
    public static List<String> linkList3;
    public static List<Object> matching;
    public static List<String> matching2;
    public static List<String> matching3;
    public static Element table;
    public static void main(String[] args) throws JSONException {
        Document doc = null;
        linkList = new ArrayList<String>();
        linkList2 = new ArrayList<String>();
        linkList3 = new ArrayList<String>();
        matching2 = new ArrayList<String>();
        matching3 = new ArrayList<String>();
        DatabaseSaver.connect();

    try {
            doc = Jsoup.connect("https://garden.org/plants/group/").get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        org.jsoup.select.Elements links = doc.select("a");
        for (Element e : links) {
            linkList.add(e.attr("abs:href"));
            Pattern pattern = Pattern.compile(regex);

            matching = linkList.stream()
                    .filter(pattern.asPredicate())
                    .collect(Collectors.toList());

        }
        for (Object x : matching) {
            try {
                doc = Jsoup.connect(x.toString()).get();
            } catch (IOException e) {
                e.printStackTrace();
            }
            org.jsoup.select.Elements links2 = doc.select("a");
            for (Element e : links2) {
                linkList2.add(e.attr("abs:href"));
                Pattern pattern = Pattern.compile(regex2);

                matching2 = linkList2.stream()
                        .filter(pattern.asPredicate())
                        .collect(Collectors.toList());
            }


        }



        int limit = 0;
        String name = "";
        ArrayList<String> items = new ArrayList<String>();
        JSONObject jsonObject = new JSONObject();



        for (Object x : matching2) {
            try {
                doc = Jsoup.connect(x.toString()).get();
                System.out.println("NEW");

            } catch (IOException e) {
                e.printStackTrace();
            }
            org.jsoup.select.Elements links3 = doc.select("a");
            for (Element e : links3) {
                linkList3.add(e.attr("abs:href"));
                Pattern pattern = Pattern.compile(regex3);

                matching3 = linkList3.stream()
                        .filter(pattern.asPredicate())
                        .collect(Collectors.toList());
            }


        }
        File file = null;
        RandomAccessFile raf= null;
        try {

            file = new File("DemoRandomAccessFile.txt");
            raf = new RandomAccessFile(file, "rw");






        } catch (IOException e) {
            System.out.println("IOException:");
            e.printStackTrace();
        }

        int seite = 0;
        for (int z = 800; z < matching3.size(); z++) {
            seite++;
            Object address = matching3.get(z);
             seite++;
            System.out.println(address.toString());



        String[] parts = null;

                try {
                    doc = Jsoup.connect(address.toString()).get();
                    Element innerCell = null;

                    //Get Name
                    Element cell = doc.select("h1").get(0);
                    if(cell.select("i").size() != 0) {
                        innerCell = cell.select("i").get(0);

                        if (cell.text().contains("\'") && cell.text().contains("(") && cell.text().indexOf("(")< cell.text().indexOf("\'") && cell.text().contains("(") && cell.text().indexOf("\'")< cell.text().indexOf(")")) {
                            name = (innerCell.text() + " " + cell.text().substring(cell.text().indexOf("\'"), cell.text().lastIndexOf("\'")));
                        } else {
                            name = innerCell.text();

                        }
                    }
                    else{
                        if (cell.text().contains("(")) {
                            name = cell.text().substring((cell.text().indexOf("("))+1 , cell.text().indexOf(")")-1);
                        }
                        else{
                            name = cell.text();
                        }

                    }






                } catch (IOException e) {
                    e.printStackTrace();
                }





            //Get Params
            int tablenumbers = doc.select("table").size();
            if (tablenumbers > 2) {
                 table = doc.select("table").get(2); //select the first table.
            }
            else if (tablenumbers > 1) {

                    table = doc.select("table").get(0);
                }
                else{
                table = null;

            } /**if (cell.text().contains("\'") && cell.text().contains("(") && cell.text().indexOf("(")< cell.text().indexOf("\'") && cell.text().contains("(") && cell.text().indexOf("\'")< cell.text().indexOf(")")) {
             name = (innerCell.text() + " " + cell.text().substring(cell.text().indexOf("\'"), cell.text().lastIndexOf("\'")));
             name = name.replaceAll("'","");



             return name;
             } else {
             name = (innerCell.text());
             name = name.replaceAll("'","");
             return name;
             }
             }
             else{
             if (cell.text().contains("(")) {
             name = (cell.text().substring((cell.text().indexOf("("))+1 , cell.text().indexOf(")")-1));
             name = name.replaceAll("'","");
             return name;
             }
             else{
             name = (cell.text());
             name = name.replaceAll("'","");
             return name;
             }
             **/



            if (table != null) {
                Elements rows = table.select("tr");
                for (int i = 0; i < rows.size(); i++) {
                    parts = rows.get(i).text().split(":");

                    if (parts.length > 1) {
                        items.add(parts[0]);
                        items.add(parts[1]);
                    }
                    if(items.size() == 2){
                        jsonObject.put(items.get(0), items.get(1));
                    }


                    items.clear();


                }
            }


            try {
                raf.write(0x0A);
                raf.writeBytes("\""+name+"\""+"\r\n");
                raf.write(0x0A);

            } catch (IOException e) {
                e.printStackTrace();
            }

            DatabaseSaver.writeJSON(name, jsonObject);
            //System.out.println("Name:"+name);

        }
        try {
            raf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

