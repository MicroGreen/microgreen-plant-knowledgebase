package com;
import static spark.Spark.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.Random;

public class Crawler {
    private DocGetter docGetter;
    private FileWriter fileWriter;
    private LinkFilter linkFilter;
    private DatabaseSaver databaseSaver;

    private List<String> linksSubpageRaw;
    private List<String> linksSubpage;
    private List<String> links;

    public Crawler(){
        int plantcounter=0;
        this.docGetter = new DocGetter("https://garden.org/plants/group/");
        this.fileWriter = new FileWriter("DemoRandomAccessFile.txt");
        this.linkFilter = new LinkFilter(this.docGetter);
        this.databaseSaver = new DatabaseSaver();
        this.databaseSaver.connect();
        this.linkFilter.setDoc(new DocGetter("https://garden.org/plants/group/"));
        this.linksSubpageRaw = this.linkFilter.getHref();
        this.linksSubpageRaw = this.linkFilter.regexFilter("^https:\\/\\/garden.org\\/plants\\/group\\/\\w", linksSubpageRaw);



        this.linksSubpage =  new ArrayList<String>();

        for (String e:linksSubpageRaw) {
            System.out.println("x");

            linkFilter = new LinkFilter(new DocGetter(e));
            List<String> temp = (this.linkFilter.getHref());
            try {

                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }

            this.linksSubpage.add((this.linkFilter.regexFilter("^https:\\/\\/garden.org\\/plants\\/browse\\/plants\\/children\\/", temp).get(0))+"popular/");
        }
        System.out.println(linksSubpage);
        this.links =  new ArrayList<String>();


        for (String e:linksSubpage) {
            linkFilter = new LinkFilter(new DocGetter(e));
            List<String> temp = (this.linkFilter.getHref());

           try {
               TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            this.links.addAll((this.linkFilter.regexFilter("^https:\\/\\/garden.org\\/plants\\/view\\/.*", temp)));
            this.links = this.removeDuplicates(links);
        }
        System.out.println(this.links);

for (int i = 0; i < links.size(); i++) {
            linkFilter = new LinkFilter(new DocGetter(links.get(i)));
            try {
                Random rand = new Random();
                int s = (int )(Math.random() * 190 + 45);
                TimeUnit.SECONDS.sleep(s);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }

    try {
        fileWriter.write(this.linkFilter.getPlantName()+":"+this.linkFilter.getPlantInformation().toString());
        databaseSaver.writeJSON(this.linkFilter.getPlantName(),this.linkFilter.getPlantInformation());
        System.out.println(this.linkFilter.getPlantName()+this.linkFilter.getPlantInformation());
    } catch (Exception e) {
        e.printStackTrace();
    }
    plantcounter++;
    System.out.println(plantcounter);
}



}
public static void main(String[] args)  {
    get("/hello", (req, res) -> "Hi I'm up!");
    get("/startCrawler", (req, res) -> "Started Crawler"+new Crawler());
    }
    public static <String> List<String> removeDuplicates(List<String> list)
    {

        // Create a new ArrayList
        ArrayList<String> newList = new ArrayList<String>();

        // Traverse through the first list
        for (String element : list) {

            // If this element is not present in newList
            // then add it
            if (!newList.contains(element)) {

                newList.add(element);
            }
        }

        // return the new list
        return newList;
    }
}
