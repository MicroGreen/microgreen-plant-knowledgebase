package com;

import com.couchbase.client.java.*;
import com.couchbase.client.java.document.json.*;
import com.couchbase.client.java.env.CouchbaseEnvironment;
import com.couchbase.client.java.env.DefaultCouchbaseEnvironment;
import com.couchbase.client.java.transcoder.JsonTranscoder;
import org.json.JSONObject;
import com.couchbase.client.java.document.JsonDocument;

import java.io.InputStream;
import java.util.List;
import java.util.Properties;

public class DatabaseSaver {
    private static Cluster cluster;
    private static Bucket bucket;
    private static String connectionStatus;
    private static String writeStatus;

    public static void connect(){
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = DatabaseSaver.class.getClassLoader().getResourceAsStream(("config.properties"));
            prop.load(input);
        } catch (Exception e) {
            e.printStackTrace();
        }

        CouchbaseEnvironment env = DefaultCouchbaseEnvironment.builder().queryTimeout(10000000).kvTimeout(10000000).searchTimeout(1000000).socketConnectTimeout(1000000000).managementTimeout(1000000000).connectTimeout(10000000).build();
        setCluster(CouchbaseCluster.create(env, prop.getProperty("databaseip")));
        getCluster().authenticate(prop.getProperty("databaseuser"), prop.getProperty("databaspasswd"));
        setBucket(getCluster().openBucket(prop.getProperty("bucket")));

        setConnectionStatus("Datenbank connected");

    }

    public static JsonObject toJson(List<String> items){
        JsonObject jsonObject =null;
        if(items.size() == 2){
            jsonObject.put(items.get(0), items.get(1));
            return jsonObject;

        }
        return null;
    }


    public static void writeJSON(String name, JSONObject jsonObject){
        String rawJson = jsonObject.toString();
        JsonTranscoder trans = new JsonTranscoder();
        try {
            JsonObject jsonObj = trans.stringToJsonObject(rawJson);
            getBucket().upsert(JsonDocument.create(name,jsonObj));
            setWriteStatus("Successfull");
        } catch (Exception e) {
            setWriteStatus("Unsuccessfull");
        }


    }


    public static Cluster getCluster() {
        return cluster;
    }

    public static void setCluster(Cluster cluster) {
        DatabaseSaver.cluster = cluster;
    }

    public static Bucket getBucket() {
        return bucket;
    }

    public static void setBucket(Bucket bucket) {
        DatabaseSaver.bucket = bucket;
    }

    public static String getWriteStatus() {
        return writeStatus;
    }

    public static void setWriteStatus(String writeStatus) {
        DatabaseSaver.writeStatus = writeStatus;
    }

    public String getConnectionStatus() {
        return connectionStatus;
    }

    public static void setConnectionStatus(String connectionStatus) {
        DatabaseSaver.connectionStatus = connectionStatus;
    }
}
