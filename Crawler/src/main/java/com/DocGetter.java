package com;

import org.jsoup.nodes.Document;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class DocGetter {
    private Document doc;
    private ArrayList<String> userAgents;
    private Boolean error;

    public DocGetter(String website){
        this.setDoc(website);
    }

    public Document getDoc() {
        return doc;
    }

    public String randomAgent(){
        Random rand = new Random();
        int randomNum = rand.nextInt((userAgents.size()-1 - 0) + 1) + 0;
        return userAgents.get(randomNum);
    }

    public void setDoc(String docS) {
        userAgents = new ArrayList<String>();
        userAgents.add("Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322)");
        userAgents.add("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0");
        userAgents.add("Mozilla/5.0 (iPad; CPU OS 10_3_3 like Mac OS X) AppleWebKit/603.3.8 (KHTML, like Gecko) Version/10.0 Mobile/14G60 Safari/602.1");
        userAgents.add("Mozilla/5.0 (Linux; U; Android 4.3; de-de; GT-I9300 Build/JSS15J) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");
        userAgents.add("Mozilla/5.0 (Linux; Android 4.3; GT-I9300 Build/JSS15J) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.98 Mobile Safari/537.36");
        userAgents.add("Opera/9.80 (Windows NT 6.1; WOW64) Presto/2.12.388 Version/12.18");


        try {
            this.doc = Jsoup.connect(docS).userAgent(randomAgent()).get();
        } catch (IOException e) {
            setError(true);
        }
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }
}
