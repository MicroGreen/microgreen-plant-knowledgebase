package com;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
public class FileWriter {
    private String path;
    private RandomAccessFile raf;
    private int curser;

    public FileWriter(String path){
        this.path = path;
        this.raf = null;
        try {

            raf = new RandomAccessFile(new File(path), "rw");
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public void write(String text){
        try {
            this.raf.writeBytes(text+"\r\n");
            this.raf.write(0x0A);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String read(int index){
        String text = "Nothing found";
        try {
            this.raf.seek(index);
            text = this.raf.readLine();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return text;
    }
}
