package com;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class LinkFilter {
    private DocGetter doc;
    private String parentPlant;
    private FileWriter filewriter;

    public LinkFilter(DocGetter doc){
        this.setDoc(doc);
        this.filewriter = new FileWriter("DemoRandomAccessFile.txt");
    }

    public Elements getLinks(){
       return this.doc.getDoc().select("a");
    }

    public  List<String> getHref(){
        Elements links = this.getLinks();
        List<String> linkList =  new ArrayList<String>();
        for (Element e : links) {
            linkList.add(e.attr("abs:href"));
        }
        return linkList;
    }

    public List<String> regexFilter(String regex, List<String> list){
        Pattern pattern = Pattern.compile(regex);
        List<String> checkedList = list.stream().filter(pattern.asPredicate()).collect(Collectors.toList());
        return checkedList;
    }

    public Elements getTables() {
        return getDoc().getDoc().select("table");
    }

    public int getTableSize(){
        int size;
        try{
            size = getTables().size();
        }catch (Exception e){
            size = 0;
        }
        return size;
    }

    public  Element getTableContent() {
        Element table = null;
        for (int i = 0; i < this.getTableSize() - 1; i++) {
            if (this.getTables().get(i).select("caption").text().contains("General Plant Information ")) {
                table = this.getTables().get(i);
                return table;
            }
        }
        return null;
    }

    public String getParentPlant(){
       String parentplant = "";
        Elements links = this.getLinks();
        List<String> linkList = new ArrayList<String>();
        for (Element e : links) {
            if (e.text().contains("See the general plant entry for")){
               DocGetter temp= new DocGetter((e.attr("abs:href")));
                Element cell = temp.getDoc().select("h1").get(0);
                if(cell.text().contains("The Main Plant entry for")){
                    parentplant =  cell.text().substring(25,(cell.text().length()));
                    break;
                }
            }
        }
        return parentplant;
    }

    public String getPlantName(){
        String name = "";
        Element cell = getDoc().getDoc().select("h1").get(0);


            if(cell.text().contains("The Main Plant entry for")){
                name =  cell.text().substring(25,(cell.text().length()));
                parentPlant = getParentPlant();
            }
            else{
                if(cell.text().contains(")") && cell.text().contains("'")) {
                    name = cell.text().substring(0, cell.text().indexOf(")") + 2);
                    parentPlant = getParentPlant();
                }
                else if(cell.text().contains(")")){
                    name= cell.text().substring(0, cell.text().indexOf(" in the "));
                    if(cell.text().contains("'")) {
                        parentPlant = getParentPlant();
                    }

                    parentPlant = getParentPlant();
                }
                else if(cell.text().contains("'")){
                    name= cell.text().substring(0, cell.text().indexOf(" in the "));
                    try {
                        parentPlant = name.substring(name.indexOf("("), name.indexOf(")")) + ")";
                    }catch (Exception e){
                        parentPlant = getParentPlant();
                    }
                }
            }


        return name;
    }
    public JSONObject getPlantInformation() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        if (this.getTableContent() != null) {
            Elements rows = this.getTableContent().select("tr");

            List<String> items = new ArrayList<String>();
            String[] parts = null;
            for (int i = 0; i < rows.size(); i++) {
                parts = rows.get(i).text().split(":");
                if (parts.length > 1) {
                    switch(parts[0].toString()){
                        case "Sun Requirements":
                            filewriter.write(parts[1].toString());
                            items.add("sunrequirements");
                            switch (parts[1].toString()){
                                case " Full Sun":  items.add("80-100"); break;
                                case " Full Sun to Partial Shade":  items.add("60-80"); break;
                                case " Partial or Dappled Shade":  items.add("30-60"); break;
                                case " Partial Shade to Full Shade":  items.add("10-30"); break;
                                case " Full Shade":  items.add("0-10"); break;
                            }
                            break;
                        case "Water Preferences":
                            items.add("waterpreference");
                            filewriter.write(parts[1].toString());
                            switch (parts[1].toString()){
                                case " In Water":  items.add("85-100"); break;
                                case " Wet":  items.add("65-85"); break;
                                case " Wet Mesic":  items.add("45-65"); break;
                                case " Mesic":  items.add("25-45"); break;
                                case " Dry Mesic":  items.add("10-25"); break;
                                case " Dry":  items.add("0-10"); break;
                                case " Mesic Dry Mesic Dry": items.add("0-45"); break;
                            }
                            break;
                        case "Minimum cold hardiness":
                            items.add("mintemp");
                            try {
                                items.add(parts[1].substring(parts[1].indexOf("to") + 3, parts[1].lastIndexOf("(")-4));
                            }catch (Exception e){
                                break;
                            }
                            break;

                            default: items.add("Nothing");
                    }

                }
                if (items.size() == 2) {
                    jsonObject.put(items.get(0), items.get(1));
                }

                items.clear();
            }
            try {
                jsonObject.put("parent", this.parentPlant);
            }catch (Exception e){
                jsonObject.put("parent",null);
            }
            jsonObject.put("name", this.getPlantName());
            return jsonObject;
        }
        return jsonObject;
    }


    public DocGetter getDoc() {
        return this.doc;
    }

    public void setDoc(DocGetter doc) {
        this.doc = doc;
    }
}
