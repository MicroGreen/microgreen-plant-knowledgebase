import com.*;
import org.json.JSONException;
import org.junit.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class JUnit5Test {

    @BeforeAll
    public static void startCrawler(){
        String[] args = {""};
         Crawler.main(args);

    }

    @Test
    public void testCrawlerUp() {
        DocGetter docGetter = new DocGetter("http://localhost:4567/hello");
        assertTrue(docGetter.getDoc().text().contains("Hi I'm up!"));
    }

    @Test
    public void testGetDocument() {
        DocGetter docGetter = new DocGetter("https://garden.org/");
        assertTrue(docGetter.getDoc().baseUri().contains("garden"));
    }

    @Test
    public void testGetDocumentInvalidUrl() {
        DocGetter docGetter = new DocGetter("https://garden.org/111");
        try {
            docGetter.getDoc();
        }
        catch(Exception e) {
            //  Block of code to handle errors
        }
        assertTrue(docGetter.getError());
    }

    @Test
    public void testSetDocument() {
        DocGetter docGetter = new DocGetter("https://garden.org/");
        docGetter.setDoc("https://garden.org/plants/view/757842/Standard-Dwarf-Bearded-Iris-Iris-Roaring-Fire/");
        assertTrue(docGetter.getDoc().baseUri().contains("757842"));
    }

    @Test
    public void testGetRandomUserAgent() {
        DocGetter docGetter = new DocGetter("https://garden.org/");
        String ua = docGetter.randomAgent();
        assertTrue(ua.contains("("));
    }
    @Test
    public void testUnequalRandomUserAgent() {
        DocGetter docGetter = new DocGetter("https://garden.org/");
        String ua1 = docGetter.randomAgent();
        String ua2 = docGetter.randomAgent();
        String ua3 = docGetter.randomAgent();
        assertTrue(ua1 != ua2 || ua2 != ua3);
    }
    @Test
    public void testGetLinks1() {
        DocGetter docGetter = new DocGetter("https://garden.org/");
        LinkFilter linkFilter = new LinkFilter(docGetter);
        assertTrue(linkFilter.getLinks().text() != "");
    }

    @Test
    public void testGetLinks2() {
        DocGetter docGetter = new DocGetter("https://garden.org/");
        LinkFilter linkFilter = new LinkFilter(docGetter);
        assertTrue(linkFilter.getLinks().text().contains("Plants"));
    }

    @Test
    public void testGetHref() {
        DocGetter docGetter = new DocGetter("https://garden.org/");
        LinkFilter linkFilter = new LinkFilter(docGetter);
        assertTrue(linkFilter.getHref().size() >= 10);
    }

    @Test
    public void testRegexFilter() {
        DocGetter docGetter = new DocGetter("https://garden.org/plants/group/");
        LinkFilter linkFilter = new LinkFilter(docGetter);
        List<String> temp = (linkFilter.getHref());
        temp = linkFilter.regexFilter("^https:\\/\\/garden.org\\/plants\\/group\\/\\w", temp);
        assertFalse(temp.contains("https://garden.org/users/join/"));
    }
    @Test
    public void testGetTables() {
        DocGetter docGetter = new DocGetter("https://garden.org");
        LinkFilter linkFilter = new LinkFilter(docGetter);
        assertFalse(linkFilter.getTables().toString().contains("Plant"));
    }
    @Test
    public void testGetTables2() {
        DocGetter docGetter = new DocGetter("https://garden.org/plants/view/530825/Peonies-Paeonia/");
        LinkFilter linkFilter = new LinkFilter(docGetter);
        assertTrue(linkFilter.getTables().toString().contains("Fruit"));
    }
    @Test
    public void testGetTableSize() {
        DocGetter docGetter = new DocGetter("https://garden.org/plants/view/530825/Peonies-Paeonia/");
        LinkFilter linkFilter = new LinkFilter(docGetter);
        assertEquals(linkFilter.getTableSize(),3);
    }

    @Test
    public void testGetTableContent() {
        DocGetter docGetter = new DocGetter("https://garden.org/plants/view/530825/Peonies-Paeonia/");
        LinkFilter linkFilter = new LinkFilter(docGetter);
        assertTrue(linkFilter.getTableContent().text().contains("Dehiscent"));
    }

    @Test
    public void testGetParentPlant1() {
        DocGetter docGetter = new DocGetter("https://garden.org/plants/view/5080/Daylily-Hemerocallis-Stella-de-Oro/");
        LinkFilter linkFilter = new LinkFilter(docGetter);
        assertTrue(linkFilter.getParentPlant().contains("Daylilies (Hemerocallis)"));
    }
    @Test
    public void testGetParentPlant2() {
        DocGetter docGetter = new DocGetter("https://garden.org/plants/view/59674/Daylily-Hemerocallis-P-E-Locas/");
        LinkFilter linkFilter = new LinkFilter(docGetter);
        assertTrue(linkFilter.getParentPlant().contains("Daylilies (Hemerocallis)"));
    }
    @Test
    public void testGetParentPlant3() {
        DocGetter docGetter = new DocGetter("https://garden.org/plants/view/20/Rose-Rosa-Double-Delight/");
        LinkFilter linkFilter = new LinkFilter(docGetter);
        assertTrue(linkFilter.getParentPlant().contains("Roses (Rosa)"));
    }
    @Test
    public void testGetParentPlantFail() {
        DocGetter docGetter = new DocGetter("https://garden.org/plants/view/181473/Daylilies-Hemerocallis/");
        LinkFilter linkFilter = new LinkFilter(docGetter);
        assertTrue(linkFilter.getParentPlant()=="");
    }
    @Test
    public void testGetPlantName1() {
        DocGetter docGetter = new DocGetter("https://garden.org/plants/view/5080/Daylily-Hemerocallis-Stella-de-Oro/");
        LinkFilter linkFilter = new LinkFilter(docGetter);
        assertTrue(linkFilter.getPlantName().contains("Daylily (Hemerocallis 'Stella de Oro')"));
    }
    @Test
    public void testGetPlantName2() {
        DocGetter docGetter = new DocGetter("https://garden.org/plants/view/79751/Parrys-Agave-Agave-parryi/");
        LinkFilter linkFilter = new LinkFilter(docGetter);
        assertTrue(linkFilter.getPlantName().contains("Parry's Agave (Agave parryi)"));
    }
    @Test
    public void testGetPlantName3() {
        DocGetter docGetter = new DocGetter("https://garden.org/plants/view/87883/Creeping-Thyme-Thymus-serpyllum/");
        LinkFilter linkFilter = new LinkFilter(docGetter);
        assertTrue(linkFilter.getPlantName().contains("Creeping Thyme (Thymus serpyllum)"));
    }
    @Test
    public void testGetPlantNameFail() {
        DocGetter docGetter = new DocGetter("https://garden.org/plants/view/20/Rose-Rosa-Double-Delight/");
        LinkFilter linkFilter = new LinkFilter(docGetter);
        assertFalse(linkFilter.getPlantName().contains("Creeping Thyme (Thymus serpyllum)"));
    }
    @Test
    public void testGetPlantInformation1() throws Exception{
        DocGetter docGetter = new DocGetter("https://garden.org/plants/view/20/Rose-Rosa-Double-Delight/");
        LinkFilter linkFilter = new LinkFilter(docGetter);
        assertEquals(linkFilter.getPlantInformation().get("sunrequirements"),"80-100");
    }

    @Test
    public void testGetPlantInformation2() throws Exception{
        DocGetter docGetter = new DocGetter("https://garden.org/plants/view/20/Rose-Rosa-Double-Delight/");
        LinkFilter linkFilter = new LinkFilter(docGetter);
        assertTrue(linkFilter.getPlantInformation().get("mintemp").toString().contains("-12.2"));
    }

    @Test
    public void testGetPlantInformation3() throws Exception{
        DocGetter docGetter = new DocGetter("https://garden.org/plants/view/658867/Egyptian-Walking-Onion-Allium-x-proliferum/");
        LinkFilter linkFilter = new LinkFilter(docGetter);
        assertEquals(linkFilter.getPlantInformation().get("sunrequirements"),"60-80");
    }
    @Test
    public void testGetPlantInformation4() throws Exception{
        DocGetter docGetter = new DocGetter("https://garden.org/plants/view/658867/Egyptian-Walking-Onion-Allium-x-proliferum/");
        LinkFilter linkFilter = new LinkFilter(docGetter);
        assertEquals(linkFilter.getPlantInformation().get("waterpreference"),"25-45");
    }
    @Test
    public void testGetPlantInformation5() throws Exception{
        DocGetter docGetter = new DocGetter("https://garden.org/plants/view/170730/African-Violet-Saintpaulia-Buckeye-Cranberry-Sparkler/");
        LinkFilter linkFilter = new LinkFilter(docGetter);
        assertTrue(linkFilter.getPlantInformation().get("mintemp").toString().contains("-3.9"));
    }
    @Test
    public void testGetPlantInformationFail1() throws Exception{
        DocGetter docGetter = new DocGetter("https://garden.org/plants/view/232959/Air-Plants-Tillandsia/");
        LinkFilter linkFilter = new LinkFilter(docGetter);
        assertFalse(linkFilter.getPlantInformation().toString().contains("mintemp"));
    }
    @Test
    public void testGetPlantInformationFail2() throws Exception{
        DocGetter docGetter = new DocGetter("https://garden.org/plants/view/85687/Cardinal-Air-Plant-Tillandsia-fasciculata/");
        LinkFilter linkFilter = new LinkFilter(docGetter);
        assertFalse(linkFilter.getPlantInformation().toString().contains("waterpreference"));
    }

    @Test
    public void testWriteFile(){
        FileWriter fileWriter = new FileWriter("test.txt");
        fileWriter.write("Hallo.");
    }

    @Test
    public void testReadFile(){
        FileWriter fileWriter = new FileWriter("test.txt");
        fileWriter.read(0).contains("Hallo.");
    }

    @Test
    public void testDatabaseConnection(){
        DatabaseSaver databaseSaver = new DatabaseSaver();
        databaseSaver.connect();
        assertEquals("Datenbank connected",databaseSaver.getConnectionStatus());
    }
    @Test
    public void testStoreJSON() throws JSONException {
        DatabaseSaver databaseSaver = new DatabaseSaver();
        databaseSaver.connect();
        DocGetter docGetter = new DocGetter("https://garden.org/plants/view/85687/Cardinal-Air-Plant-Tillandsia-fasciculata/");
        LinkFilter linkFilter = new LinkFilter(docGetter);
        databaseSaver.writeJSON(linkFilter.getPlantName(), linkFilter.getPlantInformation());
        assertEquals("Successfull",databaseSaver.getWriteStatus());
    }
    @Test
    public void testStoreJSON2() throws JSONException {
        DatabaseSaver databaseSaver = new DatabaseSaver();
        DocGetter docGetter = new DocGetter("https://garden.org/plants/view/232959/Air-Plants-Tillandsia/");
        LinkFilter linkFilter = new LinkFilter(docGetter);
        databaseSaver.writeJSON(linkFilter.getPlantName(), linkFilter.getPlantInformation());
        assertEquals("Successfull",databaseSaver.getWriteStatus());
    }



}