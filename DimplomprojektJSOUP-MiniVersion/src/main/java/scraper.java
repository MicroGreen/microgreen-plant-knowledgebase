import org.jsoup.nodes.Document;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

public class scraper {
    public static void main (String[] args){

        Elements links = null;
        try {
            links = Jsoup.connect("https://garden.org/plants/browse/plants/children/181473/popular/").get().select("a");
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (Element e : links) {
           System.out.println(e.attr("abs:href"));
        }
    }
}
