# Plant Interface 

## Description
This interface provides access for the plant-knowledgebase. It is possible to search for plants in the database and get get the information of each plant.

## Configuration 
The database needs to be configured before starting the interface.
```cd REST-Interfaces\src\main\resources```  ```config.properties```

The requirements are a couchbase database, a bucket and an user with reading privileges.

## Test
```cd REST-Interfaces```

```mvn clean test```

### Test-Report
```cd REST-Interfaces```

```mvn surefire-report:report```


## Run Interface

### Build Jar
```cd REST-Interfaces```

```mvn package```

### Execute Jar:
```cd REST-Interfaces/target```

```java -jar REST-Interface_PKB-1.0-SNAPSHOT.jar```

### Execute with Maven
```cd REST-Interfaces```

```mvn exec:java```

### Access Interpoint

Visit ```localhost:4554/search?plantname=NAME``` the name parameter can either be string or a single character. The information of a plant can be called with ```localhost:4554/getPlantinformation?plantname=NAME```.

## Deploy Spring Plant Interface  

### Deploy on Heroku 

After creating a new app in heroku named 'microgreen-plantknowledgebase' the following steps have to be done:
```heroku login```

```heroku git:clone -a microgreen-plantknowledgebase```

```git add .```

```git commit -am "your commit"```

```git subtree push --prefix REST-Interfaces heroku master```

If you have different heroku apps located on master, make sure that you change the remote before pushing.

```git remote set-url heroku https://git.heroku.com/microgreen-plantknowledgebase.git ```

### Prepare Docker
Before building the [Dockerfile](https://gitlab.com/MicroGreen/microgreen-plant-knowledgebase/REST-Interfaces/Dockerfile) it is necessary to build a new Jar File with ```mvn package```.

The docker image can be created running the following command:
```docker build --tag=plantknowledgebase .```

### Deploy on Linux-Server
After the docker image can be created it can be started on a linux server with docker.

```docker run -p 8083:4554 -d --name plantknowledgebase plantknowledgebase```

After that the Service can be visited on the sites: 
```http://microgreen.meschareth.net:8083/search?plantname=NAME``` - returns list of plants that contained search-pattern.

```http://microgreen.meschareth.net:8083/getPlantinformation?plantname=NAME``` - returns information of a plant.


