package com.microgreen.demo;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.env.CouchbaseEnvironment;
import com.couchbase.client.java.env.DefaultCouchbaseEnvironment;
import com.couchbase.client.java.query.N1qlQuery;
import com.couchbase.client.java.query.N1qlQueryResult;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;
import java.util.PropertyPermission;

@Service
@Component
public class DatabaseConnector {
    private static Cluster cluster;
    private static Bucket bucket;
    private static Properties prop;

    public DatabaseConnector(){
        prop = new Properties();
        InputStream input = null;
        try {
            input = DatabaseConnector.class.getClassLoader().getResourceAsStream(("config.properties"));
            prop.load(input);
        } catch (Exception e) {
            e.printStackTrace();
        }

        CouchbaseEnvironment env = DefaultCouchbaseEnvironment.builder().queryTimeout(10000000).kvTimeout(10000000).searchTimeout(1000000).socketConnectTimeout(1000000000).managementTimeout(1000000000).connectTimeout(10000000).keepAliveTimeout(100000000).build();
        setCluster(CouchbaseCluster.create(env, prop.getProperty("databaseip")));
        getCluster().authenticate(prop.getProperty("databaseuser"), prop.getProperty("databaspasswd"));
        setBucket(getCluster().openBucket(prop.getProperty("bucket")));

        System.out.println("Datenbank connected");
        getKeys();

    }
    public ArrayList<String> getKeys() {
        String query = "SELECT META(t).id FROM cloudplantdb t";
        N1qlQueryResult result1 = bucket.query(N1qlQuery.simple(query));
        ArrayList<String> keys = new ArrayList<String>();
        for(int i = 0; i < result1.allRows().size(); i++) {
           keys.add(result1.allRows().get(i).value().get("id").toString());
        }
        return keys;
    }
    public ArrayList<String> filterKeys(String searchparam){
        ArrayList<String> searchresult = new ArrayList<String>();
        ArrayList<String> keys = getKeys();
        for (int i = 0; i < keys.size(); i++){
            if(keys.get(i).toLowerCase().startsWith(searchparam.toLowerCase())){
                searchresult.add(keys.get(i));
            }
        }
        System.out.println(searchresult.size());
        return searchresult;
    }
    public boolean isParent(String plantkey) {
        String query = "SELECT META(t).id FROM cloudplantdb t where META(t).id =" + "'"+ plantkey+ "'" + "AND parent=''" ;
        N1qlQueryResult result1 = bucket.query(N1qlQuery.simple(query));
        if(result1.allRows().size()==1){
            return true;
        }else {
            return false;
        }
    }
    public String getParent(String child){
        String query = "SELECT parent FROM cloudplantdb t where META(t).id =" + "\""+ child+ "\"";
        N1qlQueryResult result1 = bucket.query(N1qlQuery.simple(query));
        if(result1.allRows().get(0).value().containsKey("parent")) {
            return result1.allRows().get(0).value().get("parent").toString();
        }
        return null;
    }
    public String[] getPlantinformation(String name){
        String query = "SELECT cloudplantdb.* FROM cloudplantdb where cloudplantdb.name like \""+name+"%\"";
        String[] infos = new String[5];
        N1qlQueryResult result1 = bucket.query(N1qlQuery.simple(query));

        if(result1.allRows().get(0).value().containsKey("mintemp")) {
           infos[0] = (result1.allRows().get(0).value().get("mintemp").toString());
        }else{
            infos[0] = null;
        }
        if(result1.allRows().get(0).value().containsKey("name")) {
            infos[1] = (result1.allRows().get(0).value().get("name").toString());
        }else{
            infos[1] = null;
        }
        if(result1.allRows().get(0).value().containsKey("parent")) {
            infos[2] = (result1.allRows().get(0).value().get("parent").toString());
        }else{
            infos[2] = null;
        }
        if(result1.allRows().get(0).value().containsKey("sunrequirements")) {
            infos[3] = (result1.allRows().get(0).value().get("sunrequirements").toString());
        }else{
            infos[3] = null;
        }
        if(result1.allRows().get(0).value().containsKey("waterpreference")) {
            infos[4] = (result1.allRows().get(0).value().get("waterpreference").toString());
        }else{
            infos[4] = null;
        }
        return infos;

    }

    public static Cluster getCluster() {
        return cluster;
    }

    public static void setCluster(Cluster cluster) {
        DatabaseConnector.cluster = cluster;
    }

    public static Bucket getBucket() {
        return bucket;
    }

    public static void setBucket(Bucket bucket) {
        DatabaseConnector.bucket = bucket;
    }
}
