package com.microgreen.demo;

import java.util.ArrayList;

public class Plantinformation {

    public Plantinformation() {
        this.parent = null;
        this.name = null;
        this.waterpreference = null;
        this.sunreqirements =null;
        this.mintemp = 0;
    }
    public Plantinformation(String name, String waterpreference, String sunreqirements, float mintemp) {
        this.name = name;
        this.waterpreference = waterpreference;
        this.sunreqirements = sunreqirements;
        this.mintemp = mintemp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWaterpreference() {
        return waterpreference;
    }

    public void setWaterpreference(String waterpreference) {
        this.waterpreference = waterpreference;
    }

    public String getSunreqirements() {
        return sunreqirements;
    }

    public void setSunreqirements(String sunreqirements) {
        this.sunreqirements = sunreqirements;
    }

    public float getMintemp() {
        return mintemp;
    }

    public void setMintemp(float mintemp) {
        this.mintemp = mintemp;
    }

    private String name;

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    private String parent;
    private String waterpreference;
    private String sunreqirements;
    private float mintemp;

}
