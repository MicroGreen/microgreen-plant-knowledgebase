import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;

public class TemplateClass {
    DatabaseConnector databaseConnector;
    public TemplateClass(){
         databaseConnector = new DatabaseConnector();
    }

    public ArrayList<Plantsearch> search(String name) {
        ArrayList<Plantsearch> list = new ArrayList<Plantsearch>();
        ArrayList<String> plants = databaseConnector.filterKeys(name);

        ArrayList<ArrayList> children = new ArrayList<ArrayList>();

        if(plants.size()==0){
            list.add(new Plantsearch());
            list.get(0).setParent("Nothing Found");
            return list;
        }

        boolean isIn =false;

        for (int i = 0; i < plants.size(); i++) {
            for (int j = 0; j < list.size(); j++) {
                String y =(list.get(list.size()-1).getParent());
                String z = (databaseConnector.getParent(plants.get(i)));
                if(z!=null) {
                    if (y.contains(z)) {
                        isIn = true;
                        break;
                    }
                }
            }
            if(isIn){
                if(!databaseConnector.isParent(plants.get(i))) {
                    children.get(children.size() - 1).add(plants.get(i));
                }
                isIn = false;

            }else{
                list.add(new Plantsearch());
                children.add(new ArrayList<String>());
                String parent = databaseConnector.getParent(plants.get(i));
                if(parent == null){
                    list.get(list.size() - 1).setParent(plants.get(i));

                }else {
                    list.get(list.size() - 1).setParent(databaseConnector.getParent(plants.get(i)));
                    children.get(children.size()-1).add(plants.get(i));

                }
            }

        }


        for (int i = 0; i < list.size(); i++) {
            list.get(i).setPlants(children.get(i));
        }

        return list;
    }


    public Plantinformation getPlantinformation(String name) {
        Plantinformation p = new Plantinformation();
        String[] informationArray = databaseConnector.getPlantinformation(name);
        try {
            p.setMintemp(Float.parseFloat(informationArray[0]));
        }catch (Exception e){
            p.setMintemp(0);
        }
        p.setName(informationArray[1]);
        p.setParent(informationArray[2]);
        p.setSunreqirements(informationArray[3]);
        p.setWaterpreference(informationArray[4]);
        return p;
    }
}
