import com.microgreen.demo.*;
import javax.swing.JOptionPane;
import org.junit.Before;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import org.jsoup.nodes.Document;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration
class JUnit5Test {
    private DatabaseConnector databaseConnector;

    @BeforeEach
    void initialize() {
        databaseConnector = new DatabaseConnector();

    }
    @Rule
    public ExpectedException thrown = ExpectedException.none();


    @Test
    void testGetAllPlants() {
        assertTrue(databaseConnector.getKeys().size()>100);
    }

    @Test
    void testGetParent1() {
        assertTrue(databaseConnector.getParent("African Violet (Saintpaulia 'Buckeye Cranberry Sparkler') ") != "");
    }

    @Test
    void testGetParent2() {
        assertTrue(databaseConnector.getParent("African Violet (Saintpaulia 'Buckeye Cranberry Sparkler') ").contains("African Violets (Saintpaulia)"));
    }


    @Test
    void testIsParentFalse2() {
        assertFalse(databaseConnector.isParent("Bellflower (Campanula takesimana 'Elizabeth')"));
    }
    @Test
    void testIsParentTrue() {
        assertTrue(databaseConnector.isParent("African Violets (Saintpaulia)"));
    }

    @Test
    void testIsParentFalse() {
        assertFalse(databaseConnector.isParent("African Violet (Saintpaulia 'Buckeye Cranberry Sparkler')"));
    }

    @Test
    void testGetPlantinformation1() {
        assertFalse(databaseConnector.getPlantinformation("African Violet (Saintpaulia 'Buckeye Cranberry Sparkler')").length==0);
    }




    @Test
    void testGetInfoIsUp() throws IOException{
        URL url = new URL("http://microgreen.meschareth.net:8083/getPlantinformation?plantname=Hosta%20%27Stained%20Glass%27");
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setRequestMethod("GET");
        connection.connect();
        int code = connection.getResponseCode();
        assertEquals(code,200);

    }
    @Test
    void testGetNameDeployment1() throws IOException{
        URL url = new URL("http://microgreen.meschareth.net:8083/search?plantname=daf");
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setRequestMethod("GET");
        connection.connect();
        int code = connection.getResponseCode();
        assertEquals(code,200);

    }
    @Test
    void testGetNameDeployment2() throws IOException{
        URL url = new URL("http://microgreen.meschareth.net:8083/search?plantname=tom");
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setRequestMethod("GET");
        connection.connect();
        int code = connection.getResponseCode();
        assertEquals(code,200);

    }
    @Test
    void testGetNameDeployment3() throws IOException{
        URL url = new URL("http://microgreen.meschareth.net:8083/search?plantname=d");
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setRequestMethod("GET");
        connection.connect();
        int code = connection.getResponseCode();
        assertEquals(code,200);
    }

    @Test
    void testGetNameDeploymentFail() throws IOException{
        URL url = new URL("http://microgreen.meschareth.net:8083/search?plantname=xxxxx");
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setRequestMethod("GET");
        connection.connect();
        int code = connection.getResponseCode();
        assertEquals(code,200);
    }


    @Test
    void testNoMapping() throws IOException{
        URL url = new URL("http://microgreen.meschareth.net:8082/xy");
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setRequestMethod("GET");
        connection.connect();
        int code = connection.getResponseCode();
        assertEquals(code,404);
    }

    @Test
    void testRestInterfaceIsUp() throws IOException{
        URL url = new URL("http://microgreen.meschareth.net:8083/search?plantname=da");
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setRequestMethod("GET");
        connection.connect();
        int code = connection.getResponseCode();
        assertEquals(code,200);

    }

    @Test
    void testGetPlantinformation2() {
        assertTrue((databaseConnector.getPlantinformation("African Violet (Saintpaulia 'Arapahoe')")).length>=4);
    }

    @Test
    void testGetPlantinformation3() {
        assertTrue((databaseConnector.getPlantinformation("African Violet (Saintpaulia 'Arapahoe')"))[0].contains("+7.2"));
    }
    @Test
    void testGetPlantinformation4() {
        assertTrue((databaseConnector.getPlantinformation("Banana Shrub (Magnolia figo)"))[1].contains("Banana Shrub (Magnolia figo)"));
    }
    @Test
    void testGetPlantinformation5() {
        assertTrue((databaseConnector.getPlantinformation("Banana Shrub (Magnolia figo)"))[3].contains("60-80"));
    }
    @Test
    void testGetPlantinformation6() {
        assertTrue((databaseConnector.getPlantinformation("Baptisias (Baptisia)")).length>0);
    }
    @Test
    void testGetPlantinformation7() {
        assertEquals("",(databaseConnector.getPlantinformation("Baptisias (Baptisia)")[2]));
    }


}